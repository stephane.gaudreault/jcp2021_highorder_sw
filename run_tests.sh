#!/bin/bash

config=$1

for i in $(ls -1 $config/*.ini); do name=`basename $i`;  mpirun --output-filename log_${name} -merge-stderr-to-stdout --tag-output -n 6 python3 ./main_gef.py $i  ; done
